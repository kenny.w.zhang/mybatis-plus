package com.baomidou.mybatisplus.extension.plugins.pagination.dialects;

/**
 * Firebird 数据库分页语句组装实现,SQL测试版本为4.0
 * 备注：The FIRST/SKIP and ROWS clause are non-standard alternatives
 *
 * @author cdtjj
 * @since 2022-04-26
 * @deprecated 2022-05-30
 */
@Deprecated
public class FirebirdDialect extends Oracle12cDialect {
}
